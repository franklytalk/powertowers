﻿using System;

namespace DataObjects
{
    public class Indicator
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Title { get; set; }

        public int Value { get; set; }

        public int MinValue { get; set; }

        public int MaxValue { get; set; }

    }
}
