﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjects
{
    public class PowerTower
    {
        public int Id { get; set; }

        public List<Indicator> Indicators { get; set; }
    }
}
