﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PowerTower.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Силовые башни";

            string path = Server.MapPath(@"~\Scripts\PowerTowers\src\index.html");

            ViewBag.Html = string.Empty;

            if (System.IO.File.Exists(path))
            {
                ViewBag.Html = System.IO.File.ReadAllText(path);
            }

            return View();
        }
    }
}
