﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataObjects;
using Repository;
using Microsoft.AspNet.SignalR;
using PowerTower.Hubs;

namespace PowerTower.Controllers
{
    public class TowerController : ApiController
    {
        IndicatorRepository _repository = new IndicatorRepository();
        IHubContext _hubcontext = GlobalHost.ConnectionManager.GetHubContext<TowerHub>();

        public List<DataObjects.Indicator> Get(int id)
        {
            var indicators = _repository.GetIndicators().Where(t => t.Id.Equals(id)).ToList();

            return indicators;
        }
        [Route("api/tower/{_type}")]
        public HttpResponseMessage Post(string _type, [FromBody]DataObjects.Indicator _indicator)
        {
            
            var isOk = _repository.UpdateTowerIndicator(_type, _indicator);
            var data = _repository.GetTowers();
            _hubcontext.Clients.All.SendTowers(CheckEightItem(data));
            return isOk ? new HttpResponseMessage(HttpStatusCode.OK) : new HttpResponseMessage(HttpStatusCode.Forbidden);
        }
        [Route("api/tower/{_type}")]
        public HttpResponseMessage Put(string _type, [FromBody]DataObjects.Indicator _indicator)
        {
            var isOk = _repository.SaveTowerIndicator(_type, _indicator);
            var data = _repository.GetTowers();
            _hubcontext.Clients.All.getTowers(CheckEightItem(data));
            return isOk ? new HttpResponseMessage(HttpStatusCode.Created) : new HttpResponseMessage(HttpStatusCode.Forbidden);
        }
        [Route("api/tower/{_type}")]
        public HttpResponseMessage Delete(string _type, [FromBody]DataObjects.Indicator _indicator)
        {
            var isOk = _repository.RemoveTowerIndicator(_type, _indicator);
            var data = _repository.GetTowers();
            _hubcontext.Clients.All.SendTowers(CheckEightItem(data));
            return isOk ? new HttpResponseMessage(HttpStatusCode.OK) : new HttpResponseMessage(HttpStatusCode.Forbidden);
        }

        private List<DataObjects.PowerTower> CheckEightItem(List<DataObjects.PowerTower> data)
        {
            for (var i = 1; i <= 8; i++)
            {
                if (!data.Any(t => t.Id.Equals(i)))
                {
                    data.Add(new DataObjects.PowerTower()
                    {
                        Id = i,
                        Indicators = new List<DataObjects.Indicator>()
                        { new DataObjects.Indicator(){ Title = i.ToString(), Type = "loadlevel", Id = i, MaxValue=0, MinValue=0 },
                          new DataObjects.Indicator(){ Title = i.ToString(), Type = "mass", Id = i, MaxValue=0, MinValue=0 }
                        }
                    });
                }
                else
                {
                    var tower = data.First(t => t.Id == i);
                    var indicators = tower.Indicators.OrderBy(ind => ind.Id);
                    tower.Indicators = new List<DataObjects.Indicator>
                        {
                            indicators.Where(ind => ind.Type.Equals("loadlevel"))?.LastOrDefault() ?? new DataObjects.Indicator() { Title = i.ToString(), Type = "loadlevel", Id = i, MaxValue = 0, MinValue = 0 }
                          , indicators.Where(ind => ind.Type.Equals("mass"))?.LastOrDefault() ?? new DataObjects.Indicator() { Title = i.ToString(), Type = "mass", Id = i, MaxValue = 0, MinValue = 0 }
                        };
                }
            }
            return data.OrderBy(t => t.Id).Take(8).ToList();
        }
    }
}