﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using DataObjects;
using Repository;

namespace PowerTower.Hubs
{

    public class TowerHub : Hub
    {
        IndicatorRepository _repository = new IndicatorRepository();


        public void SendTowers()
        {
           var data = _repository.GetTowers();
           Clients.All.GetTowers(CheckEightItem(data));
        }

        public void GetTowers()
        {
            var data = _repository.GetTowers();
            Clients.All.GetTowers(CheckEightItem(data));
        }

        private List<DataObjects.PowerTower> CheckEightItem(List<DataObjects.PowerTower> data)
        {
            for (var i = 1; i <= 8; i++)
            {
                if (!data.Any(t => t.Id.Equals(i)))
                {
                    data.Add(new DataObjects.PowerTower()
                    {
                        Id = i,
                        Indicators = new List<DataObjects.Indicator>()
                        { new DataObjects.Indicator(){ Title = i.ToString(), Type = "loadlevel", Id = i, MaxValue=0, MinValue=0 },
                          new DataObjects.Indicator(){ Title = i.ToString(), Type = "mass", Id = i, MaxValue=0, MinValue=0 }
                        }
                    });
                } else
                {
                    var tower = data.First(t => t.Id == i);
                    var indicators = tower.Indicators.OrderBy(ind => ind.Id);
                    tower.Indicators = new List<DataObjects.Indicator>
                        {
                            indicators.Where(ind => ind.Type.Equals("loadlevel"))?.LastOrDefault() ?? new DataObjects.Indicator() { Title = i.ToString(), Type = "loadlevel", Id = i, MaxValue = 0, MinValue = 0 }
                          , indicators.Where(ind => ind.Type.Equals("mass"))?.LastOrDefault() ?? new DataObjects.Indicator() { Title = i.ToString(), Type = "mass", Id = i, MaxValue = 0, MinValue = 0 }
                        };
                }                
            }
            return data.OrderBy(t => t.Id).Take(8).ToList();
        }
    }
}
