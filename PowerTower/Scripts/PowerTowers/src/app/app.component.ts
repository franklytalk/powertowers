import { Component, NgZone } from '@angular/core';
import { TowerService } from './tower.service';
import {Observable, Subscription} from "rxjs/Rx";
import { Tower, Indidator } from './models/Tower';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    towers: Tower[]; 
    //[{Id:number, Indidators:[{Id:number, Type:string, Title:string, Value:number, MinValue:number, MaxValue:number}]}];
  

    constructor(private zone:NgZone, private _signalRTowerService: TowerService) {
    	this.subscribeToEvents();
        
        this.towers = this._signalRTowerService.towers || new Array<Tower>();
    }

    private setData(data){
      this.towers = data;
    }

    private subscribeToEvents(): void {

        var self = this;

        this._signalRTowerService.towersReceived.subscribe(() => {
          this.zone.run(() => {
            this.towers = this._signalRTowerService.towers;
          });
        });        
    }
    
}
