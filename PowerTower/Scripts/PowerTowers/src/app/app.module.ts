import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TowerService } from './tower.service';
import { TowerComponent } from './tower/tower.component';


@NgModule({
  declarations: [
    AppComponent,
    TowerComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [TowerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
