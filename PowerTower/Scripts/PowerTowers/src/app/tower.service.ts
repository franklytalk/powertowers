import { Injectable, EventEmitter } from '@angular/core';
import { Tower, Indidator } from './models/Tower';
//declare var $: JQueryStatic;
declare var $: any;

@Injectable()
export class TowerService {
    public towers: any;
   
    public towersReceived: EventEmitter<Tower>;
    public connectionEstablished: EventEmitter<Boolean>;
    public connectionExists: Boolean;
    private server: any;
    private client: any;
    private chat: any;
    constructor() {
        this.connectionEstablished = new EventEmitter<Boolean>();
        this.towersReceived = new EventEmitter<Tower>();
        
        this.connectionExists = false;

        this.chat = $.connection.towerHub;
        this.server = this.chat.server;
        this.client = this.chat.client;
        this.registerOnServerEvents();
        this.towers = new Array<Tower>();
        this.startConnection();
    }

    private startConnection(): void {
     //   console.log('$.connection.hub.start ' + $.connection.hub.start);
         $.connection.hub.start().done((data: any) => {
            console.log('connection ID= ' + data.id);
            this.connectionEstablished.emit(true);

            this.server.getTowers();
            this.connectionExists = true;
        }).fail((error) => {
            console.log('Could not connect ' + error);
            this.connectionEstablished.emit(false);
        });
    }

    private registerOnServerEvents(): void {
        this.client.GetTowers=(data: any)=>{
            console.log('Данные: '+ JSON.stringify(data));
            this.towers = data;
            this.towersReceived.emit(data);            
        };        
        /*this.client.CheckTowers=(data: any)=>{
            this.server.getTowers();            
        };*/
    }
}