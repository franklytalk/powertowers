import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Tower, Indidator } from '../models/Tower';

@Component({
  selector: 'app-tower',
  templateUrl: './tower.component.html',
  styleUrls: ['./tower.component.css']
})
export class TowerComponent implements OnInit {
  
  loadlevel: Indidator = {Id:0, Type:'', Title:'', Value:0, MinValue:0, MaxValue:0};
  
  mass: Indidator = {Id:0, Type:'', Title:'', Value:0, MinValue:0, MaxValue:0};

  
  @Input() indicators: Indidator[];
  
  
  constructor() {
  }
  
  ngOnInit() {
  	console.log("TowerComponent"+ this.loadlevel);
    this.loadlevel = this.indicators.filter(i => i.Type === 'loadlevel')[0];
    this.mass = this.indicators.filter(i => i.Type === 'mass')[0];    
  }

}
