﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
namespace Repository
{
    public class Indicator
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public int Value { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        public int? PowerTowerId { get; set; }
        public PowerTower Tower { get; set; }
    }
}
