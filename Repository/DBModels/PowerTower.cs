﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
namespace Repository
{

    public class PowerTower
    {
        [Key]
        public int Id { get; set; }
        
        public int IdTower { get; set; }

        public ICollection<Indicator> Indicators { get; set; }

        public PowerTower()
        {
            Indicators = new List<Indicator>();
        }
    }
}
