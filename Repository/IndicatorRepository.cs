﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataObjects;
using AutoMapper;

namespace Repository
{
    public class IndicatorRepository
    {
        IMapper _mapper;
        public IndicatorRepository()
        {
            _mapper = new MapperConfiguration(init =>
            {
                init.CreateMap<Repository.PowerTower, DataObjects.PowerTower>()
                .ForMember("Id", i => i.MapFrom(p => p.IdTower))
                .ForMember("Indicators", i => i.MapFrom(p => p.Indicators));

                init.CreateMap<Repository.Indicator, DataObjects.Indicator>()
                    .ForMember("Id", i => i.MapFrom(p => p.Tower.IdTower))
                    .ForMember("Type", i => i.MapFrom(p => p.Type))
                    .ForMember("Title", i => i.MapFrom(p => p.Title))
                    .ForMember("Value", i => i.MapFrom(p => p.Value))
                    .ForMember("MinValue", i => i.MapFrom(p => p.MinValue))
                    .ForMember("MaxValue", i => i.MapFrom(p => p.MaxValue));
            }).CreateMapper();
        }

        public List<DataObjects.PowerTower> GetTowers()
        {
            List<PowerTower> towers;            

            using (PowerTowerDBContext db = new PowerTowerDBContext())
            {
                towers = db.PowerTowers.ToList();

                if(towers.Any())
                    foreach(var tower in towers) db.Entry(tower).Collection("Indicators").Load(); //Reference
            }

            return towers.Any() ? _mapper.Map<List<Repository.PowerTower>, List<DataObjects.PowerTower>>(towers)
                       : new List<DataObjects.PowerTower>();
        }

        public List<DataObjects.Indicator> GetIndicators()
        {
            List<Repository.Indicator> indicators;
            
            using (PowerTowerDBContext db = new PowerTowerDBContext())
            {
                indicators = db.Indicators.ToList();
                if (indicators.Any())
                    foreach (var indicator in indicators) db.Entry(indicator).Reference("Tower").Load();
            }

            return indicators.Any() ? _mapper.Map<List<Repository.Indicator>, List<DataObjects.Indicator>>(indicators)
                       : new List<DataObjects.Indicator>();
        }

        public bool SaveTowerIndicator(string _type, DataObjects.Indicator _indicator)
        {
            try
            {
                using (PowerTowerDBContext db = new PowerTowerDBContext())
                {
                    PowerTower tower = db.PowerTowers.FirstOrDefault(t => t.IdTower.Equals(_indicator.Id));

                    if (tower != null) { db.Dispose(); return UpdateTowerIndicator(_type, _indicator); }

                    tower = new PowerTower() { IdTower = _indicator.Id };
                    db.Indicators.Add(new Indicator() {
                        Title = _indicator.Title,
                        Value = _indicator.Value,
                        MinValue = _indicator.MinValue,
                        MaxValue = _indicator.MaxValue,
                        Type = _type,
                        Tower = tower
                    });

                    db.SaveChanges();
                }             

            } catch(Exception e)
            {
                return false;
            }

            return true;
        }

        public bool UpdateTowerIndicator(string _type, DataObjects.Indicator _indicator)
        {
            try
            {
                using (PowerTowerDBContext db = new PowerTowerDBContext())
                {
                    var tower = db.PowerTowers.FirstOrDefault(t => t.IdTower.Equals(_indicator.Id));

                    if (tower == null) { db.Dispose();  return SaveTowerIndicator(_type, _indicator); }

                    db.Entry(tower).Collection("Indicators").Load();

                    var indicators = tower.Indicators.Where(i => i.Type.Equals(_type)).ToList();

                    if (indicators.Count > 0) { 
                        foreach (Indicator indicator in indicators)
                        {
                            indicator.Title = _indicator.Title;
                            indicator.Value = _type.Equals("loadlevel") ? new Random().Next(0, 100) : new Random().Next(0, _indicator.MaxValue);
                            indicator.MinValue = _indicator.MinValue;
                            indicator.MaxValue = _indicator.MaxValue;
                        }
                    } else
                    {
                        db.Indicators.Add(new Indicator()
                        {
                            Title = _indicator.Title,
                            Value = _type.Equals("loadlevel") ? new Random().Next(0, 100) : new Random().Next(0, _indicator.MaxValue),
                            MinValue = _indicator.MinValue,
                            MaxValue = _indicator.MaxValue,
                            Type = _type,
                            PowerTowerId = tower.Id
                        });                        
                    }
                    db.SaveChanges();
                }                
            }
            catch(Exception e)
            {
                return false;
            }

            return true;
        }

        public bool RemoveTowerIndicator(string _type, DataObjects.Indicator _indicator)
        {
            try
            {
                using (PowerTowerDBContext db = new PowerTowerDBContext())
                {
                    var tower = db.PowerTowers.FirstOrDefault(t => t.IdTower.Equals(_indicator.Id));

                    if (tower == null) { db.Dispose(); return false; }

                    var indicators = tower.Indicators.Where(i => i.Type.Equals(_type)).ToList();

                    if (indicators.Count > 0)
                    {
                        db.Indicators.RemoveRange(indicators);

                        if (indicators.Count == 1)
                            db.PowerTowers.Remove(tower);
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
    }
}