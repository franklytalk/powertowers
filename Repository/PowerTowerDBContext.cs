﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
 
namespace Repository
{
    public class PowerTowerDBContext : DbContext
    {
        public PowerTowerDBContext()
            : base("DefaultConnection")
        { }


        public DbSet<Indicator> Indicators { get; set; }

        public DbSet<PowerTower> PowerTowers { get; set; }
    }

    
}
